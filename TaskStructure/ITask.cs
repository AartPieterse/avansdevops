﻿using AvansDevops.SideFunctions;

namespace AvansDevops.TaskStructure
{
    public abstract class ITask
    {
        protected Project project;

        protected string title;
        protected string description;

        protected IUser? developer;

        protected IState state;

        protected IState todoState;
        protected IState doingState;
        protected IState readyForTestingState;
        protected IState testingState;
        protected IState testedState;
        protected IState doneState;
        protected IState finalState;

        public Project GetProject()
        {
            return this.project;
        }

        public string GetTitle()
        {
            return this.title;
        }

        public string GetDescription()
        {
            return this.description;
        }

        public void SetDescription(string description)
        {
            this.description = description;
        }

        public IUser GetDeveloper()
        {
            return developer;
        }

        public void SetDeveloper(IUser developer)
        {
            this.developer = developer;
        }


        public virtual void SetState(IState state)
        {
            this.state = state;
        }

        public virtual void AddTask(ITask task)
        {
            throw new InvalidOperationException();
        }

        // State change methods
        public void TodoState()
        {
            this.state.ToTodoState();
        }

        public void DoingState()
        {
            this.state.ToDoingState();
        }

        public void ReadyForTestingState()
        {
            this.state.ToReadyForTestingState();
        }

        public void TestingState()
        {
            this.state.ToTestingState();
        }

        public void TestedState()
        {
            this.state.ToTestedState();
        }

        public void DoneState()
        {
            this.state.ToDoneState();
        }

        public void FinalState()
        {
            this.state.ToFinalState();
        }

        // State setter
        public virtual IState GetState()
        {
            return this.state;
        }

        // State getters
        public virtual IList<ITask> GetChildren()
        {
            throw new InvalidOperationException();
        }

        public virtual IState GetTodoState()
        {
            return todoState;
        }

        public virtual IState GetDoingState()
        {
            return doingState;
        }

        public virtual IState GetReadyForTestingState()
        {
            return readyForTestingState;
        }

        public virtual IState GetTestingState()
        {
            return testingState;
        }

        public virtual IState GetTestedState()
        {
            return testedState;
        }

        public virtual IState GetDoneState()
        {
            return doneState;
        }

        public virtual IState GetFinalState()
        {
            return finalState;
        }

        // Exectute method
        public abstract IList<ITask> Execute();
    }
}
