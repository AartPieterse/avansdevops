﻿using AvansDevops.SideFunctions;

namespace AvansDevops.TaskStructure
{
    public class Project
    {
        private readonly string _id;
        private string _name;
        private string _description;
        private readonly IUser _creator;

        private readonly IList<IUser> _members;
        private readonly IList<IUser> _testers;

        private IList<ITask> _tasks;

        private readonly EmailService _memberNotificationService;
        private readonly EmailService _testerNotificationService;

        public Project(string name, IUser creator) : this (name, creator, new List<ITask>(), new List<IUser>(), new List<IUser>())
        {

        }

        public Project(string name, IUser creator, IList<ITask> tasks, IList<IUser> members, IList<IUser> testers)
        {
            _id = new Random().Next().ToString();
            _name = name;
            _description = "";
            _creator = creator;

            _members = members;
            _testers = testers;

            _tasks = tasks;

            _memberNotificationService = new EmailService();
            _testerNotificationService = new EmailService();
        }

        public string GetId()
        {
            return _id;
        }

        public string GetName()
        {
            return _name;
        }

        public void SetName(string name)
        {
            _name = name;
        }

        public string GetDescription()
        {
            return _description;
        }

        public void SetDescription(string description)
        {
            _description = description;
        }

        public IUser GetCreator()
        {
            return _creator;
        }

        public IList<IUser> GetMembers()
        {
            return _members;
        }

        public void AddMember(IUser user)
        {
            _members.Add(user);

            _memberNotificationService.Subscribe(user);
        }

        public IList<IUser> GetTesters()
        {
            return _testers;
        }

        public void AddTester(IUser user)
        {
            _testers.Add(user);

            _testerNotificationService.Subscribe(user);
        }

        public IList<ITask> GetTasks()
        {
            return _tasks;
        }

        public void UpdateTasks(List<ITask> tasks)
        {
            this._tasks = tasks;
        }
    }
}
