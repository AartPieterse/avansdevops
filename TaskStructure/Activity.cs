﻿using AvansDevops.SideFunctions;
using AvansDevops.TaskStructure.States;

namespace AvansDevops.TaskStructure
{
    public class Activity : ITask
    {
        public Activity(Project project, string title, string description) : this(project, title, description, null)
        {

        }

        public Activity(Project project, string title, string description, IUser developer)
        {
            this.project = project;

            this.title = title;
            this.description = description;

            this.developer = developer;

            this.todoState = new TodoState(this);
            this.doingState = new DoingState(this);
            this.readyForTestingState = new ReadyForTestingState(this);
            this.testingState = new TestingState(this);
            this.testedState = new TestingState(this);
            this.doneState = new DoneState(this);
            this.finalState = new FinalState(this);

            this.state = todoState;
        }

        public override IList<ITask> Execute()
        {
            return new List<ITask> { this };
        }
    }
}
