﻿using AvansDevops.SideFunctions;
using AvansDevops.TaskStructure.States;

namespace AvansDevops.TaskStructure
{
    public class Task : ITask
    {
        private IList<ITask> _children;

        public Task(Project project, string title, string description) : this(project, title, description, null)
        {

        }


        public Task(Project project, string title, string description, IUser developer)
        {
            this.project = project;

            this.title = title;
            this.description = description;

            this.developer = developer;

            this.todoState = new TodoState(this);
            this.doingState = new DoingState(this);
            this.readyForTestingState = new ReadyForTestingState(this);
            this.testingState = new TestingState(this);
            this.testedState = new TestingState(this);
            this.doneState = new DoneState(this);
            this.finalState = new FinalState(this);

            this.state = todoState;

            this._children = new List<ITask>();
        }

        public override void AddTask(ITask task)
        {
            this._children.Add(task);
        }

        public override IList<ITask> GetChildren()
        {
            return this._children;
        }

        public override IList<ITask> Execute()
        {
            IEnumerable<ITask> result = new List<ITask>();

            result = result.Concat(new List<ITask>(){ this });

            foreach (ITask task in this._children)
            {
                result = result.Concat(task.Execute());
            }

            return result.ToList();
        }
    }
}
