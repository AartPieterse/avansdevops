﻿
namespace AvansDevops.TaskStructure.States
{
    public class TodoState : IState
    {
        private ITask _task;

        public TodoState(ITask task)
        {
            _task = task;
        }

        public void ToDoingState()
        {
            this._task.SetState(this._task.GetDoingState());
        }

        public void ToDoneState()
        {
            throw new InvalidOperationException();
        }

        public void ToTodoState()
        {
            throw new InvalidOperationException();
        }

        public void ToFinalState()
        {
            throw new InvalidOperationException();
        }

        public void ToReadyForTestingState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestedState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestingState()
        {
            throw new InvalidOperationException();
        }
    }
}
