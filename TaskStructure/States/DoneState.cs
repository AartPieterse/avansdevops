﻿
namespace AvansDevops.TaskStructure.States
{
    public class DoneState : IState
    {
        private ITask _task;

        public DoneState(ITask task)
        {
            this._task = task;
        }

        public void ToDoingState()
        {
            throw new InvalidOperationException();
        }

        public void ToDoneState()
        {
            throw new InvalidOperationException();
        }

        public void ToTodoState()
        {
            this._task.SetState(this._task.GetTodoState());
        }

        public void ToFinalState()
        {
            foreach (ITask task in this._task.GetChildren())
            {
                if (task.GetState() != task.GetDoneState())
                {
                    return;
                }
            }

            this._task.SetState(this._task.GetFinalState());
        }

        public void ToReadyForTestingState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestedState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestingState()
        {
            throw new InvalidOperationException();
        }
    }
}
