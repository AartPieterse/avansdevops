﻿
using AvansDevops.SideFunctions;

namespace AvansDevops.TaskStructure.States
{
    public class DoingState : IState
    {
        private ITask _task;

        public DoingState(ITask task)
        {
            _task = task;
        }

        public void ToDoingState()
        {
            throw new InvalidOperationException();
        }

        public void ToDoneState()
        {
            throw new InvalidOperationException();
        }

        public void ToTodoState()
        {
            throw new InvalidOperationException();
        }

        public void ToFinalState()
        {
            throw new InvalidOperationException();
        }

        public void ToReadyForTestingState()
        {
            _task.SetState(this._task.GetReadyForTestingState());

            foreach (IUser tester in _task.GetProject().GetTesters())
            {
                tester.UpdateSubscribers("Task is ready for testing!", "Task: " + this._task.GetTitle());
            }
        }

        public void ToTestedState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestingState()
        {
            throw new InvalidOperationException();
        }
    }
}
