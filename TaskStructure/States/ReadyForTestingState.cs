﻿
namespace AvansDevops.TaskStructure.States
{
    public class ReadyForTestingState : IState
    {
        private ITask _task;

        public ReadyForTestingState(ITask task)
        {
            _task = task;
        }

        public void ToDoingState()
        {
            throw new InvalidOperationException();
        }

        public void ToDoneState()
        {
            throw new InvalidOperationException();
        }

        public void ToTodoState()
        {
            throw new InvalidOperationException();
        }

        public void ToFinalState()
        {
            throw new InvalidOperationException();
        }

        public void ToReadyForTestingState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestedState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestingState()
        {
            this._task.SetState(this._task.GetTestingState());
        }
    }
}
