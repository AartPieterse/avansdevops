﻿
namespace AvansDevops.TaskStructure.States
{
    public class TestingState : IState
    {
        private ITask _task;

        public TestingState(ITask task)
        {
            this._task = task;
        }

        public void ToDoingState()
        {
            this._task.SetState(this._task.GetDoingState());  
        }

        public void ToDoneState()
        {
            throw new InvalidOperationException();
        }

        public void ToTodoState()
        {
            throw new InvalidOperationException();
        }

        public void ToFinalState()
        {
            throw new InvalidOperationException();
        }

        public void ToReadyForTestingState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestedState()
        {
            this._task.SetState(this._task.GetTestedState());
        }

        public void ToTestingState()
        {
            throw new InvalidOperationException();
        }
    }
}
