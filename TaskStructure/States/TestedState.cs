﻿
namespace AvansDevops.TaskStructure.States
{
    public class TestedState : IState
    {
        private ITask _task;

        public TestedState(ITask task)
        {
            _task = task;
        }

        public void ToDoingState()
        {
            throw new InvalidOperationException();
        }

        public void ToDoneState()
        {
            foreach (ITask task in this._task.GetChildren())
            {
                if (task.GetState() != task.GetDoneState())
                {
                    return;
                }
            }

            this._task.SetState(this._task.GetDoneState());
        }

        public void ToTodoState()
        {
            throw new InvalidOperationException();
        }

        public void ToFinalState()
        {
            throw new InvalidOperationException();
        }

        public void ToReadyForTestingState()
        {
            this._task.SetState(this._task.GetReadyForTestingState());
        }

        public void ToTestedState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestingState()
        {
            throw new InvalidOperationException();
        }
    }
}
