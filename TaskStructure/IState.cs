﻿
namespace AvansDevops.TaskStructure
{
    public interface IState
    {
        void ToDoingState();

        void ToDoneState();

        void ToFinalState();

        void ToReadyForTestingState();

        void ToTestedState();

        void ToTestingState();

        void ToTodoState();
    }
}
