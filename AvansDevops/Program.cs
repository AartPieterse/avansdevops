﻿using AvansDevops.DbRepository;
using AvansDevops.Project;
using AvansDevops.Project.ConcreteClasses;
using DbRepository.Models;
using Project;
using Project.ConcreteClasses;

namespace AvansDevops
{
    class Program
    {
        private static IRepo _database;

        static void Main(string[] args)
        {
            _database = new SqlRepository("connection//:string");

            IList<User> members = GetMembers();

            // Create users & testers
            IUser larryProductOwner = new User("Larry Lieshout", "larrylieshout@mail.com");
            IUser henkScrumMaster = new User("Henk van Dam", "henkvandam@mail.com");
            IUser harryMember = new User("Harry Zwart", "harryzwart@mail.com");
            IUser jessyMember = new User("Jessy Visscher", "jessyvisscher@mail.com");

            IUser sarahTester = new User("Sarah van de Berg", "sarahvdberg@mail.com");
            IUser billyTester = new User("Billy Amadeus", "billyamadeus@mail.com");

            // Create project and add members
            ScrumProject project = new ScrumProject("Avans Devops Project", larryProductOwner, henkScrumMaster);

            project.AddMember(harryMember);
            project.AddMember(jessyMember);

            project.AddTester(sarahTester);
            project.AddTester(billyTester);

            // Create backlog structure
            BacklogItemBase addDbTask = new BacklogItem(project, "Add DB", "Add a database as underlying datastorage for the project", henkScrumMaster);
            BacklogItemBase addMongoDbTask = new BacklogItem(project, "Add MongoDB", "Add a MongoDB for the storage of Tasks", harryMember);
            BacklogItemBase addSqlDbTask = new BacklogItem(project, "Add Sql DB", "Add a SQL database for the storage of user information", jessyMember);

            BacklogItemBase createAtlasHostActivity = new Activity(project, "Create MongoDB Atlas", "Fix a hosting provider for an online database", harryMember);

            // A developer is not necessary in the assignment ...
            BacklogItemBase writeSqlScriptActivity = new Activity(project, "Write SQL script", "Write the structure of the DB in a SQL script");

            // ... it can later be added to an backlog item
            writeSqlScriptActivity.SetDeveloper(sarahTester);

            // Add backlog items to the project
            addMongoDbTask.AddTask(createAtlasHostActivity);
            addSqlDbTask.AddTask(writeSqlScriptActivity);

            addDbTask.AddTask(addMongoDbTask);
            addDbTask.AddTask(addSqlDbTask);

            // (this method completely re-sets the task structure of the project)
            project.UpdateTasks(new List<BacklogItemBase> { addDbTask });

            // Print the tasks to the console
            foreach (BacklogItemBase task in project.GetBacklog())
            {
                foreach (BacklogItemBase item in task.Execute())
                {
                    Console.WriteLine(item.GetTitle());
                }
            }

            // Add sprint to project
            Sprint firstSprint = new Sprint(project, "sprint name");
            project.AddSprint(firstSprint);

            // Set backlog of the sprint
            firstSprint.SetSprintBacklog(new List<BacklogItemBase> { addDbTask });

            // Upgrade the state of the activity --> the testers should get an update here
            IList<BacklogItemBase> backlog = firstSprint.GetSprintBacklog();
            BacklogItemBase sprintItem = backlog.First().GetChildren().First();
            sprintItem.DoingState();
            sprintItem.ReadyForTestingState();

            createAtlasHostActivity.DoingState();
            createAtlasHostActivity.ReadyForTestingState();

            // Upgrade state further            
            createAtlasHostActivity.TestingState();
            createAtlasHostActivity.TestedState();

            


        }

        public static IList<User> GetMembers()
        {
            List<User> users = new();

            foreach (UserModel um in _database.GetUsers())
            {
                users.Add(MapToUserObject(um));
            }

            return users;
        }

        public static User MapToUserObject(UserModel userModel)
        {
            return new User(userModel.Name, userModel.Email);
        }
    }
}