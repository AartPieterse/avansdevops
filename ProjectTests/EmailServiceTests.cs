﻿using AvansDevops.Project;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Project.NotificationService;

namespace Project.Tests
{
    [TestClass]
    public class EmailServiceTests
    {

        [TestMethod]
        public void ShouldAddUserToSubscriberList()
        {
            var emailService = new EmailService();

            emailService.Subscribe(new User("henk", "henk@mail.com"));

            Assert.AreEqual("henk@mail.com", emailService.GetSubscribers().First().GetEmailAddress());
        }

        public void NotARealTest()
        {
            var emailService = new Mock<EmailService>();

            emailService.Setup(m => m.Subscribe(new User("name", "email")));

            emailService.Verify(m => m.GetSubscribers().Count(), "");


            EmailService ems = new EmailService();
            ems.Subscribe(new User("henk", "henk@mail.com"));

            IUser user = ems.GetSubscribers().First();
            user.GetEmailAddress();

            // Assert.Equals("d", user.GetEmailAddress());
        }
    }
}