﻿using AvansDevops.Project.ConcreteClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Project;
using Project.ConcreteClasses;

namespace ProjectTests
{
    [TestClass]
    public class ActivityTests
    {
        [TestMethod]
        public void ShouldReturnListWithSelfOnExecute()
        {
            var project = new Mock<ScrumProject>("name", new User("prod owner", "prodowner@mail.com"), new User("scrum master", "scrummaster@mail.com"));

            Activity activity = new Activity(project.Object, "title", "description text");

            IList<BacklogItemBase> returnList = activity.Execute();

            Assert.IsInstanceOfType(returnList, typeof(List<BacklogItemBase>));

            Assert.AreEqual(1, returnList.Count);
        }
    }
}
