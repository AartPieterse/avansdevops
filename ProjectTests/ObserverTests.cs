﻿using AvansDevops.Project.ConcreteClasses;
using AvansDevops.Project.States;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Project;

namespace ProjectTests
{
    [TestClass]
    public class ObserverTests
    {
        private readonly Mock<ScrumProject> _projectMock = new Mock<ScrumProject>("project name", new User("name", "name@mail"), new User("name", "name@mail"));

        [TestMethod]
        public void DiscussionCannotBeChangedWhenItemIsInFinalState_16()
        {
            var backlogMock = new Mock<BacklogItem>(_projectMock.Object, "item title", "item description");
            var discussion = "Discussion";

            backlogMock.Object.AddDiscussion(discussion);

            backlogMock.Object.SetState(backlogMock.Object.GetFinalState());

            backlogMock.Object.ChangeDiscussion("This discussion should not be changed");

            Assert.AreEqual("Discussion", discussion);
        }

        [TestMethod]
        public void AddedDiscussionShouldNotifyEveryone()
        {
            var backlogMock = new Mock<BacklogItem>(_projectMock.Object, "item title", "item description");
            var discussion = "Discussion";

            // Initialize stringWriter and reader to read from the console
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            // Adding discussion to project
            backlogMock.Object.AddDiscussion(discussion);

            // Initialize String Reader AFTER state is changed (stringwriter.ToString() prints out all lines)
            var stringReader = new StringReader(stringWriter.ToString());

            Assert.AreEqual("Failed to send email of (Discussion added to backlog thread! Discussion)", stringReader.ReadLine());
        }
    }
}
