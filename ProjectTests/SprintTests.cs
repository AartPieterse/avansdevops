﻿using AvansDevops.Project.ConcreteClasses;
using AvansDevops.Project.Rapporten;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Project;
using Project.ConcreteClasses;

namespace ProjectTests
{
    [TestClass]
    public class SprintTests
    {
        private readonly Mock<ScrumProject> projectMock = new Mock<ScrumProject>("name", new User("", ""), new User("", ""));

        [TestMethod]
        public void StartDateCanNotBeChangedToPastDate_1()
        {
            Sprint sprint = new Sprint(projectMock.Object, "project name");

            DateTime startDate = new DateTime(2023, 12, 02);

            sprint.SetStartDate(startDate);
            Assert.AreEqual(startDate, sprint.StartDate);

            // StartDate should not have been changed
            sprint.SetStartDate(new DateTime(2023, 02, 01));
            Assert.AreEqual(startDate, sprint.StartDate);
        }
        [TestMethod]
        public void EndDateCanNotBeChangedToDateBeforeStartDate_2()
        {
            Sprint sprint = new Sprint(projectMock.Object, "project name");

            DateTime correctEndDate = new DateTime(2023, 05, 20);
            DateTime faultyEndDate = new DateTime(2023, 05, 01);

            sprint.SetStartDate(new DateTime(2023, 05, 03));
            sprint.SetEndDate(correctEndDate);
            Assert.AreEqual(correctEndDate, sprint.EndDate);

            // EndDate should not have been changed
            sprint.SetEndDate(faultyEndDate);
            Assert.AreNotEqual(faultyEndDate, sprint.EndDate);
        }

        [TestMethod]
        public void BacklogCanNotBeChangedWhileSprintIsRolling_3()
        {
            Sprint sprint = new Sprint(projectMock.Object, "project name");

            BacklogItemBase item = new BacklogItem(projectMock.Object, "baclogitem title", "backlogitem description");
            item.AddTask(new BacklogItem(projectMock.Object, "backlogitem 2 title", "backlogitem 2 description"));

            sprint.SetSprintBacklog(new List<BacklogItemBase> { item });

            sprint.RollingState();
            Assert.AreEqual(sprint.GetRollingState(), sprint.GetState());

            BacklogItemBase item2 = new BacklogItem(projectMock.Object, "backlogitem 3 title", "backlogitem 3 description");
            sprint.SetSprintBacklog(new List<BacklogItemBase> { item, item2 });

            Assert.AreEqual(1, sprint.GetSprintBacklog().Count);
        }

        [TestMethod]
        public void SprintShouldGenerateRapport()
        {
            // Arrange
            Sprint sprint = new Sprint(projectMock.Object, "project name", "project description", DateTime.MinValue, DateTime.MaxValue, null, new PdfRapport("Header", "Body"));
            
            // Act
            try
            {
                sprint.GetRapportGenerator().GenerateRapport();
            }
            catch (Exception)
            {
                // If an exception is thrown, the test will fail
                Assert.Fail("An exception was thrown");
            }
        }
    }
}
