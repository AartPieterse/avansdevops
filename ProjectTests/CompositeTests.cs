﻿using AvansDevops.Project;
using AvansDevops.Project.ConcreteClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Project;
using Project.ConcreteClasses;

namespace ProjectTests
{
    [TestClass]
    public class CompositeTests
    {
        private readonly Mock<ScrumProject> _projectMock = new Mock<ScrumProject>("project name", new User("name", "name@mail"), new User("name", "name@mail"));

        [TestMethod]
        public void BacklogItemCanOnlyBeLinkedToOneDeveloper_11()
        {
            IUser developer1 = new User("name", "name@gmail.com");
            BacklogItemBase item = new BacklogItem(_projectMock.Object, "baclogitem title", "backlogitem description", developer1);

            IUser developer2 = new User("name", "name@gmail.com");
            item.SetDeveloper(developer2);

            Assert.AreEqual(item.GetDeveloper(), developer2);
        }
    }
}
