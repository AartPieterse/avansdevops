﻿using AvansDevops.Project.ConcreteClasses;
using AvansDevops.Project.States;
using AvansDevops.Project;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Project;
using Git;
using Project.ConcreteClasses;

namespace ProjectTests
{
    [TestClass]
    public class StateTests
    {
        private readonly Mock<ScrumProject> _projectMock = new Mock<ScrumProject>("project name", new User("name", "name@mail"), new User("name", "name@mail"));

        [TestMethod]
        public void ShouldSendNotificationIfStateIsSetToReadyForTesting_4()
        {
            var backlogMock = new Mock<BacklogItem>(_projectMock.Object, "item title", "item description");
            DoingState doingState = new DoingState(backlogMock.Object);

            // project needs testers to send notifications to
            IUser tester1 = new User("henk", "henk@mail.com");
            _projectMock.Object.AddTester(tester1);

            // Redirect console output to a String Writer
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            doingState.ToReadyForTestingState();

            Assert.AreEqual("Failed to send email of (An item is ready for testing! Backlog item: item title)", stringWriter.ToString().Trim());
        }

        [TestMethod]
        public void ScrumMasterShouldGetNotificationIfItemIsPutBackInTodo_5()
        {
            var backlogMock = new Mock<BacklogItem>(_projectMock.Object, "item title", "item description");
            DoneState doneState = new(backlogMock.Object);

            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            doneState.ToTodoState();

            Assert.AreEqual("Failed to send email of (An item is set back to To Do! Backlog item: item title)", stringWriter.ToString().Trim());
        }

        [TestMethod]
        public void SprintShouldStartPipelineWhenWhenFinished_6()
        {
            // Initialize sprint
            Sprint sprint = new(_projectMock.Object, "project name");
            _projectMock.Object.AddSprint(sprint);
            // Initialize pipeline
            IList<string> packages = new List<string>() { "sonarcloud"};
            Pipeline pipeline = new Pipeline(_projectMock.Object, new Code("Hello World!"), packages, "DeployUrl");
            // We only want to test the build stage, then we know that it is running
            pipeline.Test = false;
            pipeline.Analyze = false;
            pipeline.Deploy = false;
            pipeline.Utility = false;

            // Add pipeline to the sprint
            sprint.SetPipeline(pipeline);

            // Set state to finished
            sprint.SetState(sprint.GetFinishedState());

            // Initialize stringWriter and reader to read from the console
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            // Set state to pipeline so that we can check if the console printed out "build package", meaning that it is running
            sprint.InPipelineState();

            // Check if console printed expected line
            Assert.AreEqual("build package", stringWriter.ToString().Trim());
        }

        [TestMethod]
        public void CancelledSprintShouldNotifyScrumMaster_7()
        {
            // Initialize sprint
            Sprint sprint = new(_projectMock.Object, "project name");
            sprint.SetState(sprint.GetFinishedState());

            // Initialize stringWriter and reader to read from the console
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            // Set state to cancelled
            sprint.CancelledState();

            // Initialize String Reader AFTER state is changed (stringwriter.ToString() prints out both lines, due to both productowner and scrummaster being informed about cancelled string)
            var stringReader = new StringReader(stringWriter.ToString());

            // Check if console printed expected line
            Assert.AreEqual("Failed to send email of (Sprint cancelled The current sprint is set to 'Cancelled'.)", stringReader.ReadLine());
        }

        [TestMethod]
        public void RunnedPipelineCannotBeChanged_8()
        {
            // Initialize sprint
            Sprint sprint = new(_projectMock.Object, "project name");
            sprint.SetState(sprint.GetInPipelineState());

            // Initialize pipeline
            IList<string> packages = new List<string>() { "sonarcloud" };
            Pipeline pipeline = new Pipeline(_projectMock.Object, new Code("Hello World!"), packages, "DeployUrl");

            // Set pipelines that give error to false
            pipeline.Test = false;
            pipeline.Analyze = false;
            pipeline.Deploy = false;
            pipeline.Utility = false;

            // Start Pipeline
            pipeline.InitiatePipeline();

            Assert.ThrowsException<ArgumentException>(() => sprint.SetPipeline(pipeline));
        }

        [TestMethod]
        public void FailedPipelineShouldNotifyScrummaster_9()
        {
            // Initialize sprint and pipeline
            IList<string> packages = new List<string>() { "sonarcloud" };
            Pipeline pipeline = new Pipeline(_projectMock.Object, new Code("Hello World!"), packages, "DeployUrl");
            pipeline.setError(false);

            Sprint sprint = new(_projectMock.Object, "project name");
            sprint.SetPipeline(pipeline);
            sprint.SetState(sprint.GetFinishedState());

            _projectMock.Object.AddSprint(sprint);

            // Set pipelines that give error to false
            pipeline.Test = false;
            pipeline.Analyze = false;
            pipeline.Deploy = false;
            pipeline.Utility = false;

            // Initialize stringWriter and reader to read from the console
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            // Start Pipeline
            pipeline.InitiatePipeline();

            // Initialize String Reader AFTER state is changed (stringwriter.ToString() prints out all lines)
            var stringReader = new StringReader(stringWriter.ToString());

            // Check if console printed expected line
            Assert.AreEqual("Failed to send email of (Pipeline error Pipeline: " + pipeline  + ")", stringReader.ReadLine());

        }

        [TestMethod]
        public void BacklogCannotBeSetToDoneIfNotAllSubtasksAreCompleted_12()
        {
            // Initialize Backlog item and set on tested stage. Also initialize child item
            BacklogItemBase item = new BacklogItem(_projectMock.Object, "backlogitem title", "backlogitem description");
            item.SetState(item.GetTestedState());
            BacklogItemBase child = new BacklogItem(_projectMock.Object, "backlogitem subtask", "backlogitem description");

            // Adding child backlog item
            item.AddTask(child);

            // Check if setting state to done returns exception as expected
            Assert.ThrowsException<ArgumentException>(() => item.DoneState());
        }

        [TestMethod]
        public void BacklogItemSetToDoneShouldNotifyTesters_13()
        {
            // Initialize tester
            IUser tester1 = new User("name", "name@gmail.com");

            // Add to project
            _projectMock.Object.AddTester(tester1);

            // Initialize backlog item and set on tested stage
            BacklogItemBase item = new BacklogItem(_projectMock.Object, "backlogitem title", "backlogitem description");
            item.SetState(item.GetTestedState());

            // Initialize stringWriter and reader to read from the console
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            // Set item to done state
            item.DoneState();

            // Initialize String Reader AFTER state is changed (stringwriter.ToString() prints out all lines)
            var stringReader = new StringReader(stringWriter.ToString());

            // Check if console printed expected line
            Assert.AreEqual("Failed to send email of (An item is ready for testing! Backlog item: backlogitem title)", stringReader.ReadLine());

        }

        [TestMethod]
        public void BacklogItemCantBeSetBackToToDone_14()
        {
            // Initialize Backlog item
            BacklogItemBase item = new BacklogItem(_projectMock.Object, "baclogitem title", "backlogitem description");
            // Setting state to testing, because this state can't go back to To Do
            item.SetState(item.GetDoingState());

            // Check if setting state to To Do returns exception as expected
            Assert.ThrowsException<InvalidOperationException>(() => item.TodoState());
        }

        
    }
}
