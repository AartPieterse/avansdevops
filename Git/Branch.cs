﻿namespace Git
{
    public class Branch
    {
        public string Name { get; }
        public DateTime LastUpdated { get; private set; }

        public IList<Commit> Commits { get; }

        public Branch(string name, Commit commit)
        {
            Name = name;
            LastUpdated = DateTime.Now;
        }

        public Code GetCode()
        {
            return Commits.Last().Code;
        }

        public void Push(Commit commit)
        {
            this.LastUpdated = DateTime.Now;

            this.Commits.Add(commit);
        }
    }
}
