﻿namespace Git
{
    public class Commit
    {
        public string DeveloperId { get; }
        public string CommitMessage { get; }
        public Code Code { get; }
        public DateTime Date { get; }

        public Commit(string developer, string commitMessage, Code code)
        {
            DeveloperId = developer;
            CommitMessage = commitMessage;
            Code = code;
            Date = DateTime.Now;
        }
    }
}
