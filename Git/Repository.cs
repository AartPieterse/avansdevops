﻿namespace Git
{
    public class Repository
    {
        private IList<Branch> _branches;
        public Branch MainBranch { get; set; }

        public Repository(Branch mainBranch)
        {
            this._branches = new List<Branch> { mainBranch };
            this.MainBranch = mainBranch;
        }

        public IList<Branch> GetBranches()
        {
            return this._branches;
        }

        public Code GetCodeOfMainBranch()
        {
            return this.MainBranch.GetCode();
        }

        public Code GetCode(string branchName)
        {
            return _branches.First<Branch>(b => b.Name == branchName).GetCode();
        }
    }
}
