﻿namespace Git
{
    public class Code
    {
        private string _content;

        public Code(string content)
        {
            _content = content;
        }

        public string GetContent()
        {
            return _content;
        }

        public void SetContent(string content)
        {
            this._content = content;
        }
    }
}
