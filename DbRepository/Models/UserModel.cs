﻿namespace DbRepository.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public UserModel(string id, string name, string email)
        {
            Id = id;
            Name = name;
            Email = email;
        }
    }
}
