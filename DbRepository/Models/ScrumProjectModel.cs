﻿namespace DbRepository.Models
{
    public class ScrumProjectModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ScrumProjectModel(string id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
        }
    }
}
