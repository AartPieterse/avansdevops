﻿using DbRepository.Models;

namespace AvansDevops.DbRepository
{
    public interface IRepo
    {
        void SaveUser(string name, string email);

        UserModel GetUser(string id);

        IList<UserModel> GetUsers();

        void SaveScrumProject(ScrumProjectModel project);

        void SaveTask();

        ScrumProjectModel GetScrumProject(string id);
    }
}
