﻿using DbRepository.Models;

namespace AvansDevops.DbRepository
{
    public class MongoRepository : IRepo
    {
        private readonly string _databaseConnection;

        public MongoRepository(string databaseConnection)
        {
            _databaseConnection = databaseConnection;
        }

        private void Connect()
        {
            // Connect to MongoDB with 'connectionString'
        }

        public void SaveTask()
        {
            
        }

        public void SaveUser(string name, string email)
        {
        }

        public UserModel GetUser(string id)
        {
            return new UserModel("0001", "Henk van Dam", "henkvdam@mail.com");
        }

        public IList<UserModel> GetUsers()
        {
            IList<UserModel> users = new List<UserModel>();
            users.Add(new UserModel("0001", "Henk van Dam", "henkvdam@mail.com"));
            users.Add(new UserModel("0002", "Harry Zwart", "harryzwart@mail.com"));
            users.Add(new UserModel("0003", "Sarah van de Berg", "sarahvdberg@mail.com"));
            users.Add(new UserModel("0004", "Billy Amadeus", "billyamadeus@mail.com"));
            users.Add(new UserModel("0005", "Jessy Visscher", "jessyvisscher@mail.com"));

            return users;
        }

        public void SaveScrumProject(ScrumProjectModel project)
        {

        }

        public ScrumProjectModel GetScrumProject(string id)
        {
            return new ScrumProjectModel("0001", "Scrum Project", "Project for some organisation");
        }
    }
}