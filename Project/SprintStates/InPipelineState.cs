﻿namespace Project.SprintStates
{
    public class InPipelineState : ISprintState
    {
        private readonly Sprint _sprint;

        public InPipelineState(Sprint sprint)
        {
            _sprint = sprint;
        }

        public void ToCancelledState()
        {
            this._sprint.GetProject().SendProductOwnerNotification("Sprint cancelled", "The current sprint is set to 'Cancelled'.");
            this._sprint.GetProject().SendScrumMasterNotification("Sprint cancelled", "The current sprint is set to 'Cancelled'.");

            this._sprint.SetState(this._sprint.GetCancelledState());
        }

        public void ToFinishedState()
        {
            throw new NotImplementedException();
        }

        public void ToInPipelineState(Pipeline pipeline)
        {
            throw new NotImplementedException();
        }

        public void ToPreState()
        {
            throw new NotImplementedException();
        }

        public void ToReleasedState()
        {
            this._sprint.SetState(this._sprint.GetReleasedState());
        }

        public void ToReviewedState()
        {
            throw new NotImplementedException();
        }

        public void ToRollingState()
        {
            throw new NotImplementedException();
        }
    }
}
