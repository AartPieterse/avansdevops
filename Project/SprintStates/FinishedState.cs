﻿namespace Project.SprintStates
{
    public class FinishedState : ISprintState
    {
        private readonly Sprint _sprint;

        public FinishedState(Sprint sprint)
        {
            _sprint = sprint;
        }

        public void ToCancelledState()
        {
            this._sprint.GetProject().SendProductOwnerNotification("Sprint cancelled", "The current sprint is set to 'Cancelled'.");
            this._sprint.GetProject().SendScrumMasterNotification("Sprint cancelled", "The current sprint is set to 'Cancelled'.");

            this._sprint.SetState(this._sprint.GetCancelledState());
        }

        public void ToFinishedState()
        {
            throw new NotImplementedException();
        }

        public void ToInPipelineState(Pipeline pipeline)
        {               
            this._sprint.SetState(this._sprint.GetInPipelineState());

            pipeline?.InitiatePipeline();
        }

        public void ToPreState()
        {
            throw new NotImplementedException();
        }

        public void ToReleasedState()
        {
            throw new NotImplementedException();
        }

        public void ToReviewedState()
        {
            throw new NotImplementedException();
        }

        public void ToRollingState()
        {
            throw new NotImplementedException();
        }
    }
}
