﻿namespace Project.SprintStates
{
    public class ReleasedState : ISprintState
    {
        private readonly Sprint _sprint;

        public ReleasedState(Sprint sprint)
        {
            _sprint = sprint;
        }

        public void ToCancelledState()
        {
            throw new NotImplementedException();
        }

        public void ToFinishedState()
        {
            throw new NotImplementedException();
        }

        public void ToInPipelineState(Pipeline pipeline)
        {
            throw new NotImplementedException();
        }

        public void ToPreState()
        {
            throw new NotImplementedException();
        }

        public void ToReleasedState()
        {
            throw new NotImplementedException();
        }

        public void ToReviewedState()
        {
            this._sprint.SetState(this._sprint.GetReviewedState());
        }

        public void ToRollingState()
        {
            throw new NotImplementedException();
        }
    }
}
