﻿namespace Project.SprintStates
{
    public class ReviewedState : ISprintState
    {
        private readonly Sprint _sprint;

        public ReviewedState(Sprint sprint)
        {
            _sprint = sprint;
        }

        public void ToCancelledState()
        {
            throw new NotImplementedException();
        }

        public void ToFinishedState()
        {
            throw new NotImplementedException();
        }

        public void ToInPipelineState(Pipeline pipeline)
        {
            throw new NotImplementedException();
        }

        public void ToPreState()
        {
            throw new NotImplementedException();
        }

        public void ToReleasedState()
        {
            throw new NotImplementedException();
        }

        public void ToReviewedState()
        {
            throw new NotImplementedException();
        }

        public void ToRollingState()
        {
            throw new NotImplementedException();
        }
    }
}
