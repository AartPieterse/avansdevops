﻿namespace Project.SprintStates
{
    public interface ISprintState
    {
        void ToCancelledState();

        void ToFinishedState();

        void ToInPipelineState(Pipeline pipeline);

        void ToPreState();

        void ToReleasedState();

        void ToReviewedState();

        void ToRollingState();
    }
}
