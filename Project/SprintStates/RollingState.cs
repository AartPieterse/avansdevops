﻿namespace Project.SprintStates
{
    public class RollingState : ISprintState
    {
        private readonly Sprint _sprint;

        public RollingState(Sprint sprint)
        {
            _sprint = sprint;
        }

        public void ToCancelledState()
        {
            throw new NotImplementedException();
        }

        public void ToFinishedState()
        {
            this._sprint.SetState(this._sprint.GetFinishedState());
        }

        public void ToInPipelineState(Pipeline pipeline)
        {
            throw new NotImplementedException();
        }

        public void ToPreState()
        {
            throw new NotImplementedException();
        }

        public void ToReleasedState()
        {
            throw new NotImplementedException();
        }

        public void ToReviewedState()
        {
            throw new NotImplementedException();
        }

        public void ToRollingState()
        {
            throw new NotImplementedException();
        }
    }
}
