﻿using AvansDevops.Project;
using Project.BacklogItemStates;

namespace Project.ConcreteClasses
{
    public abstract class BacklogItemBase
    {
        protected ScrumProject project;

        protected string title;
        protected string description;

        protected IUser? developer;

        protected IBacklogItemState state;

        protected IBacklogItemState todoState;
        protected IBacklogItemState doingState;
        protected IBacklogItemState readyForTestingState;
        protected IBacklogItemState testingState;
        protected IBacklogItemState testedState;
        protected IBacklogItemState doneState;
        protected IBacklogItemState finalState;

        public ScrumProject GetProject()
        {
            return project;
        }

        public string GetTitle()
        {
            return title;
        }

        public string GetDescription()
        {
            return description;
        }

        public void SetDescription(string description)
        {
            this.description = description;
        }

        public IUser? GetDeveloper()
        {
            if (developer != null)
            {
                return developer;
            }

            return null;
        }

        public void SetDeveloper(IUser developer)
        {
            this.developer = developer;
        }


        public virtual void SetState(IBacklogItemState state)
        {
            this.state = state;
        }

        public virtual void AddTask(BacklogItemBase task)
        {
            throw new InvalidOperationException();
        }

        // State change methods
        public void TodoState()
        {
           state.ToTodoState();
        }

        public void DoingState()
        {
            state.ToDoingState();
        }

        public void ReadyForTestingState()
        {
            state.ToReadyForTestingState();
        }

        public void TestingState()
        {
            state.ToTestingState();
        }

        public void TestedState()
        {
            state.ToTestedState();
        }

        public void DoneState()
        {
            state.ToDoneState();
        }

        public void FinalState()
        {
            state.ToFinalState();
        }

        // Current state getter
        public virtual IBacklogItemState GetState()
        {
            return state;
        }

        // States getters
        public virtual IList<BacklogItemBase> GetChildren()
        {
            throw new InvalidOperationException();
        }

        public virtual IBacklogItemState GetTodoState()
        {
            return todoState;
        }

        public virtual IBacklogItemState GetDoingState()
        {
            return doingState;
        }

        public virtual IBacklogItemState GetReadyForTestingState()
        {
            return readyForTestingState;
        }

        public virtual IBacklogItemState GetTestingState()
        {
            return testingState;
        }

        public virtual IBacklogItemState GetTestedState()
        {
            return testedState;
        }

        public virtual IBacklogItemState GetDoneState()
        {
            return doneState;
        }

        public virtual IBacklogItemState GetFinalState()
        {
            return finalState;
        }

        // Exectute method
        public abstract IList<BacklogItemBase> Execute();
    }
}
