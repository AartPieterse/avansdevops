﻿
using AvansDevops.Project.States;
using Project;
using Project.BacklogItemDiscussion;
using Project.ConcreteClasses;

namespace AvansDevops.Project.ConcreteClasses
{
    public class BacklogItem : BacklogItemBase, IBackLogItemObserver
    {
        private List<IObserver> _observers;

        private IList<BacklogItemBase> _children;
        public List<string> Discussions;

        public BacklogItem(ScrumProject project, string title, string description) : this(project, title, description, null)
        {

        }


        public BacklogItem(ScrumProject project, string title, string description, IUser developer)
        {
            this.project = project;

            this.title = title;
            this.description = description;

            this.developer = developer;

            todoState = new TodoState(this);
            doingState = new DoingState(this);
            readyForTestingState = new ReadyForTestingState(this);
            testingState = new TestingState(this);
            testedState = new TestedState(this);
            doneState = new DoneState(this);
            finalState = new FinalState(this);

            state = todoState;

            _children = new List<BacklogItemBase>();
            Discussions = new List<string>();
            _observers = new List<IObserver>();
        }

        public override void AddTask(BacklogItemBase task)
        {
            _children.Add(task);
        }

        public void AddDiscussion(string discussion)
        {
            if(this.state == GetFinalState())
            {
                throw new ArgumentException("Adding discussions in final state is not allowed!");
            }

            this.Discussions.Add(discussion);

            this.project.SendEveryoneNotifications("Discussion added to backlog thread!", discussion);
        }

        public void ChangeDiscussion(string discussion)
        {
            if(this.state == GetFinalState())
            {
                throw new ArgumentException("Adding discussions in final state is not allowed!");
            }

            int index = this.Discussions.FindIndex(x => x.Equals(discussion));
            
            if(index != -1) this.Discussions[index] = discussion;

            this.project.SendEveryoneNotifications("Discussion added to backlog thread!", discussion);
        }

        public override IList<BacklogItemBase> GetChildren()
        {
            return _children;
        }

        public override IList<BacklogItemBase> Execute()
        {
            IEnumerable<BacklogItemBase> result = new List<BacklogItemBase>();

            result = result.Concat(new List<BacklogItemBase>() { this });

            foreach (BacklogItemBase task in _children)
            {
                result = result.Concat(task.Execute());
            }

            return result.ToList();
        }

        public void Attach(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void Notify()
        {
            foreach(var observer in _observers) observer.Update(this);
        }
    }
}
