﻿
using AvansDevops.Project.States;
using Project;
using Project.ConcreteClasses;

namespace AvansDevops.Project.ConcreteClasses
{
    public class Activity : BacklogItemBase
    {
        public Activity(ScrumProject project, string title, string description) : this(project, title, description, null)
        {

        }

        public Activity(ScrumProject project, string title, string description, IUser developer)
        {
            this.project = project;

            this.title = title;
            this.description = description;

            this.developer = developer;

            todoState = new TodoState(this);
            doingState = new DoingState(this);
            readyForTestingState = new ReadyForTestingState(this);
            testingState = new TestingState(this);
            testedState = new TestingState(this);
            doneState = new DoneState(this);
            finalState = new FinalState(this);

            state = todoState;
        }

        public override IList<BacklogItemBase> Execute()
        {
            return new List<BacklogItemBase> { this };
        }
    }
}
