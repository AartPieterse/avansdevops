﻿using Git;

namespace Project
{
    public class Pipeline
    {
        private ScrumProject _project { get; set; }
        public Code Code { get; set; }
        public IList<string> Packages { get; set; }
        public string DeployUrl { get; set; }

        public bool Build { get; set; } = true;
        public bool Test { get; set; } = true;
        public bool Analyze { get; set; } = true;
        public bool Deploy { get; set; } = true;
        public bool Utility { get; set; } = true;

        private bool _noError = true;

        public Pipeline(ScrumProject project) 
        {
            this._project = project;
        }

        public Pipeline(ScrumProject project, Code code, IList<string> packages, string deployUrl)
        {
            this._project = project;
            this.Code = code;
            this.Packages = packages;
            this.DeployUrl = deployUrl;
        }

        public void InitiatePipeline()
        {
            if (!_noError) _project.SendScrumMasterNotification("Pipeline error", "Pipeline: " + this);

            if (Build && _noError) DoBuild();

            if (Test && _noError)  DoTest();

            if (Analyze && _noError) DoAnalyze();

            if (Deploy && _noError) DoDeploy();

            if (Utility && _noError) DoUtility();
        }

        public void setError(bool error)
        {
            this._noError = error;
        }

        public void DoBuild()
        {
            foreach (var package in Packages)
            {
                Console.WriteLine("build package");
            }
        }

        public void DoTest()
        {
            Console.WriteLine("Finding tests...");

            Console.WriteLine("Running tests...");
        }

        public void DoAnalyze()
        {

        }

        public void DoDeploy()
        {
            Console.WriteLine("Deploy to " + DeployUrl);

            this._project?.GetCurrentSprint()?.SetState(_project.GetCurrentSprint().GetReleasedState());
        }

        public void DoUtility()
        {

        }
    }
}
