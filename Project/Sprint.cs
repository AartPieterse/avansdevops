﻿using Project.ConcreteClasses;
using Project.NotificationService;
using Project.Rapporten;
using Project.SprintStates;

namespace Project
{
    public class Sprint
    {
        private readonly ScrumProject _project;

        public string Name { get; set; }
        public string Description { get; set; } 
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }

        private IList<BacklogItemBase> _sprintBacklog;

        private Pipeline? _pipeline;

        private RapportBase _rapport;
        
        private ISprintState _state;

        private readonly ISprintState _cancelledState;
        private readonly ISprintState _finishedState;
        private readonly ISprintState _inPipelineState;
        private readonly ISprintState _preState;
        private readonly ISprintState _releasedState;
        private readonly ISprintState _reviewedState;
        private readonly ISprintState _rollingState;

        public Sprint(ScrumProject project, string name) : this(project, name, "no description provided", new DateTime(), new DateTime(), new List<BacklogItemBase>(), null) { }

        public Sprint(
            ScrumProject project,
            string name,
            string description,
            DateTime startDate,
            DateTime endDate,
            IList<BacklogItemBase> sprintBacklog,
            RapportBase rapport
            )
        {
            this._project = project;

            this.Name = name;
            this.Description = description;
            this.StartDate = startDate;
            this.EndDate = endDate;

            this._sprintBacklog = sprintBacklog;

            this._rapport = rapport;

            this._cancelledState = new CancelledState(this);
            this._finishedState = new FinishedState(this);
            this._inPipelineState = new InPipelineState(this);
            this._preState = new PreState(this);
            this._releasedState = new ReleasedState(this);
            this._reviewedState = new ReviewedState(this);
            this._rollingState = new RollingState(this);

            this._state = this._preState;
        }

        public RapportBase GetRapportGenerator()
        {
            return this._rapport;
        }

        public void CreateRapport()
        {
            _rapport.GenerateRapport();
        }

        public void SetStartDate(DateTime date)
        {
            if (date > DateTime.Now)            
                this.StartDate = date;
            
        }

        public void SetEndDate(DateTime date)
        {
            if (date > this.StartDate)
                this.EndDate = date;

        }

        // Sprint backlog methods
        public IList<BacklogItemBase> GetSprintBacklog()
        {
            return this._sprintBacklog;
        }

        public void SetSprintBacklog(IList<BacklogItemBase> sprintBacklog)
        {
            if (this._state == this._preState)
                this._sprintBacklog = sprintBacklog;

        }

        // Pipeline get and set
        public Pipeline? GetPipeline()
        {
            return _pipeline;
        }

        public void SetPipeline(Pipeline pipeline)
        {
            if (this._state == this._inPipelineState) throw new ArgumentException();

            this._pipeline = pipeline;
        }

        public ScrumProject GetProject()
        {
            return this._project;
        }

        // State setter
        public void SetState(ISprintState state)
        {
            if (this._state == this._inPipelineState)
                return;

            this._state = state;
        }

        // State change methods
        public void CancelledState()
        {
            this._state.ToCancelledState();
        }

        public void FinishedState()
        {
            this._state.ToFinishedState();
        }

        public void InPipelineState()
        {
            this._state.ToInPipelineState(_pipeline);
        }

        public void PreState()
        {
            this._state.ToPreState();
        }

        public void ReleasedState()
        {
            this._state.ToReleasedState();
        }

        public void ReviewedState()
        {
            this._state.ToReviewedState();
        }

        public void RollingState()
        {
            this._state.ToRollingState();
        }

        // Current state getter
        public ISprintState GetState()
        {
            return this._state;
        }

        // State getters
        public ISprintState GetCancelledState()
        {
            return this._cancelledState;
        }

        public ISprintState GetFinishedState()
        {
            return this._finishedState;
        }

        public ISprintState GetInPipelineState()
        {
            return this._inPipelineState;
        }

        public ISprintState GetPreState()
        {
            return this._preState;
        }

        public ISprintState GetReleasedState()
        {
            return this._releasedState;
        }

        public ISprintState GetReviewedState()
        {
            return this._reviewedState;
        }

        public ISprintState GetRollingState()
        {
            return this._rollingState;
        }
    }
}
