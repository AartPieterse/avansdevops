﻿namespace Project.BacklogItemDiscussion
{
    internal class DiscussionObserver : IObserver
    {
        public void Update(IBackLogItemObserver backlogItem)
        {
            Console.WriteLine("New Discussion added to backlog item");
        }
    }
}
