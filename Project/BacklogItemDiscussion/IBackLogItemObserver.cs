﻿namespace Project.BacklogItemDiscussion
{
    public interface IBackLogItemObserver
    {
        void Attach(IObserver observer);
        void Detach(IObserver observer);
        void Notify();
    }
}
