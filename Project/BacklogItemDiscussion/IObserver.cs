﻿namespace Project.BacklogItemDiscussion
{
    public interface IObserver
    {
        void Update(IBackLogItemObserver backLogItem);
    }
}
