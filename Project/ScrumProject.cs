﻿using AvansDevops.DbRepository;
using AvansDevops.Project;
using Git;
using Project.ConcreteClasses;
using Project.NotificationService;


namespace Project
{
    public class ScrumProject
    {
        public GitFacade Git;

        private readonly string _id;
        private string _name;
        private string _description;

        // Personages
        private readonly IUser _productOwner;
        private readonly IUser _scrumMaster;
        private IList<IUser> _developers;
        private IList<IUser> _testers;

        // Sprint data
        private IList<BacklogItemBase> _backlog;
        private IList<Sprint> _sprints;

        // Declare the notification services
        private readonly EmailService _productOwnerNotificationService;
        private readonly EmailService _scrumMasterNotificationService;
        private readonly EmailService _memberNotificationService;
        private readonly EmailService _testerNotificationService;

        private readonly IRepo _databaseConnection;

        public ScrumProject(string name, IUser productOwner, IUser scrumMaster) : 
            this(name, productOwner, scrumMaster, new List<BacklogItemBase>(), new List<IUser>(), new List<IUser>(), new SqlRepository("")) 
                { }

        public ScrumProject(
            string name, 
            IUser productOwner, 
            IUser scrumMaster, 
            IList<BacklogItemBase> backlog, 
            IList<IUser> developers,
            IList<IUser> testers,
            IRepo databaseConnection
            )
        {
            Git = new GitFacade();

            _id = new Random().Next().ToString();
            _name = name;
            _description = "";

            _productOwner = productOwner;
            _scrumMaster = scrumMaster;
            _developers = developers;
            _testers = testers;

            _backlog = backlog;

            _sprints = new List<Sprint>();

            // Initialize EmailServices
            _productOwnerNotificationService = new EmailService();
            _productOwnerNotificationService.Subscribe(this._productOwner);

            _scrumMasterNotificationService = new EmailService();
            _scrumMasterNotificationService.Subscribe(this._scrumMaster);

            _memberNotificationService = new EmailService();
            _testerNotificationService = new EmailService();

            _databaseConnection = databaseConnection;
        }

        public string GetId()
        {
            return _id;
        }

        public string GetName()
        {
            return _name;
        }

        public void SetName(string name)
        {
            _name = name;
        }

        public string GetDescription()
        {
            return _description;
        }

        public void SetDescription(string description)
        {
            _description = description;
        }

        public IUser GetScrumMaster()
        {
            return _scrumMaster;
        }

        public IList<IUser> GetMembers()
        {
            return _developers;
        }

        public void AddMember(IUser user)
        {
            _developers.Add(user);

            _memberNotificationService.Subscribe(user);
        }

        public IList<IUser> GetTesters()
        {
            return _testers;
        }

        public void AddTester(IUser user)
        {
            _testers.Add(user);

            _testerNotificationService.Subscribe(user);
        }

        // Tasks
        public IList<BacklogItemBase> GetBacklog()
        {
            return _backlog;
        }

        public void UpdateTasks(List<BacklogItemBase> backlog)
        {
            _backlog = backlog;
        }

        // Sprints
        public void AddSprint(Sprint sprint)
        {
            _sprints.Add(sprint);
        }

        public Sprint GetLatestSprint()
        {
            return _sprints.Last();
        }

        public Sprint? GetCurrentSprint()
        {
            return _sprints.First(s => s.GetState() == s.GetRollingState());
        }

        public void SendEveryoneNotifications(string subject, string body)
        {
            SendProductOwnerNotification(subject, body);
            SendScrumMasterNotification(subject, body);
            SendTestersNotifications(subject, body);
        }

        public void SendProductOwnerNotification(string subject, string body) 
        { 
            this._productOwnerNotificationService.NotifySubscribers(subject, body);
        }

        public void SendScrumMasterNotification(string subject, string body)
        {
            this._scrumMasterNotificationService.NotifySubscribers(subject, body);
        }

        public void SendTestersNotifications(string subject, string body)
        {
            this._testerNotificationService.NotifySubscribers(subject, body);
        }

        // Data connection
        public IRepo GetDatabaseConnection()
        {
            return this._databaseConnection;
        }
    }
}
