﻿using AvansDevops.Project;
using Git;

namespace Project
{
    public class GitFacade
    {
        private Branch Branch;

        public GitFacade() { }


        public Branch Init()
        {
            Branch = new Branch("master", null);

            return Branch;
        }

        public Commit Commit(string message, string data, IUser developer)
        {
            Code code = new Code(data);

            return new Commit(developer.GetName(), message, code);
        }

        public void Push(IList<Commit> commits)
        {
            foreach (Commit commit in commits)
            {
                this.Branch.Push(commit);
            }
        }

        public Code Pull(Branch branch)
        {
            return branch.GetCode();
        }

        public Code Sync(IList<Commit> commits, Branch branch, Code code)
        {
            // Pull
            Code pullCode = branch.GetCode();

            // Push
            this.Push(commits);

            return pullCode;
        }

        public Branch Checkout(Repository repo, string branchName)
        {
            return repo.GetBranches().First(b => b.Name == branchName);
        }
    }
}
