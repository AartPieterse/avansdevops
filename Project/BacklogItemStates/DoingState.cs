﻿
using Project.BacklogItemStates;
using Project.ConcreteClasses;

namespace AvansDevops.Project.States
{
    public class DoingState : IBacklogItemState
    {
        private readonly BacklogItemBase _item;

        public DoingState(BacklogItemBase item)
        {
            _item = item;
        }

        public void ToDoingState()
        {
            throw new InvalidOperationException();
        }

        public void ToDoneState()
        {
            throw new InvalidOperationException();
        }

        public void ToTodoState()
        {
            throw new InvalidOperationException();
        }

        public void ToFinalState()
        {
            throw new InvalidOperationException();
        }

        public void ToReadyForTestingState()
        {
            _item.SetState(this._item.GetReadyForTestingState());

            _item.GetProject().SendTestersNotifications("An item is ready for testing!", "Backlog item: " + this._item.GetTitle());
        }

        public void ToTestedState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestingState()
        {
            throw new InvalidOperationException();
        }
    }
}
