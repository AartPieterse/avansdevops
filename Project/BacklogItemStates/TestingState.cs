﻿
using Project.BacklogItemStates;
using Project.ConcreteClasses;

namespace AvansDevops.Project.States
{
    public class TestingState : IBacklogItemState
    {
        private readonly BacklogItemBase _item;

        public TestingState(BacklogItemBase item)
        {
            this._item = item;
        }

        public void ToDoingState()
        {
            this._item.SetState(this._item.GetDoingState());
        }

        public void ToDoneState()
        {
            throw new InvalidOperationException();
        }

        public void ToTodoState()
        {
            throw new InvalidOperationException();
        }

        public void ToFinalState()
        {
            throw new InvalidOperationException();
        }

        public void ToReadyForTestingState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestedState()
        {
            this._item.SetState(this._item.GetTestedState());
        }

        public void ToTestingState()
        {
            throw new InvalidOperationException();
        }
    }
}
