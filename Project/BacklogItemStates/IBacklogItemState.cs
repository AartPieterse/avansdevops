﻿namespace Project.BacklogItemStates
{
    public interface IBacklogItemState
    {
        void ToDoingState();

        void ToDoneState();

        void ToFinalState();

        void ToReadyForTestingState();

        void ToTestedState();

        void ToTestingState();

        void ToTodoState();
    }
}
