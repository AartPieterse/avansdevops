﻿
using Project.BacklogItemStates;
using Project.ConcreteClasses;

namespace AvansDevops.Project.States
{
    public class FinalState : IBacklogItemState
    {
        private readonly BacklogItemBase _item;

        public FinalState(BacklogItemBase item)
        {
            _item = item;
        }

        public void ToDoingState()
        {
            throw new InvalidOperationException();
        }

        public void ToDoneState()
        {
            throw new InvalidOperationException();
        }

        public void ToTodoState()
        {
            throw new InvalidOperationException();
        }

        public void ToFinalState()
        {
            throw new InvalidOperationException();
        }

        public void ToReadyForTestingState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestedState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestingState()
        {
            throw new InvalidOperationException();
        }
    }
}
