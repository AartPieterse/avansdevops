﻿
using Project.BacklogItemStates;
using Project.ConcreteClasses;

namespace AvansDevops.Project.States
{
    public class DoneState : IBacklogItemState
    {
        private readonly BacklogItemBase _item;

        public DoneState(BacklogItemBase item)
        {
            this._item = item;
        }

        public void ToDoingState()
        {
            throw new InvalidOperationException();
        }

        public void ToDoneState()
        {
            throw new InvalidOperationException();
        }

        public void ToTodoState()
        {
            this._item.SetState(this._item.GetTodoState());

            _item.GetProject().GetScrumMaster().UpdateSubscribers("An item is set back to To Do!", "Backlog item: " + this._item.GetTitle());
        }

        public void ToFinalState()
        {
            foreach (BacklogItemBase task in this._item.GetChildren())
            {
                if (task.GetState() != task.GetDoneState())
                {
                    return;
                }
            }

            this._item.SetState(this._item.GetFinalState());
        }

        public void ToReadyForTestingState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestedState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestingState()
        {
            throw new InvalidOperationException();
        }
    }
}
