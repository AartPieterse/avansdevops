﻿
using Project.BacklogItemStates;
using Project.ConcreteClasses;

namespace AvansDevops.Project.States
{
    public class TestedState : IBacklogItemState
    {
        private readonly BacklogItemBase _item;

        public TestedState(BacklogItemBase item)
        {
            _item = item;
        }

        public void ToDoingState()
        {
            throw new InvalidOperationException();
        }

        public void ToDoneState()
        {
            foreach (BacklogItemBase item in this._item.GetChildren())
            {
                if (item.GetState() != item.GetDoneState())
                {
                    throw new ArgumentException();
                }
            }

            _item.SetState(this._item.GetDoneState());

            _item.GetProject().SendTestersNotifications("An item is ready for testing!", "Backlog item: " + this._item.GetTitle());
        }

        public void ToTodoState()
        {
            throw new InvalidOperationException();
        }

        public void ToFinalState()
        {
            throw new InvalidOperationException();
        }

        public void ToReadyForTestingState()
        {
            this._item.SetState(this._item.GetReadyForTestingState());
        }

        public void ToTestedState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestingState()
        {
            throw new InvalidOperationException();
        }
    }
}
