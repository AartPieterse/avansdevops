﻿
using Project.BacklogItemStates;
using Project.ConcreteClasses;

namespace AvansDevops.Project.States
{
    public class ReadyForTestingState : IBacklogItemState
    {
        private readonly BacklogItemBase _item;

        public ReadyForTestingState(BacklogItemBase item)
        {
            _item = item;
        }

        public void ToDoingState()
        {
            throw new InvalidOperationException();
        }

        public void ToDoneState()
        {
            throw new InvalidOperationException();
        }

        public void ToTodoState()
        {
            throw new InvalidOperationException();
        }

        public void ToFinalState()
        {
            throw new InvalidOperationException();
        }

        public void ToReadyForTestingState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestedState()
        {
            throw new InvalidOperationException();
        }

        public void ToTestingState()
        {
            this._item.SetState(this._item.GetTestingState());
        }
    }
}
