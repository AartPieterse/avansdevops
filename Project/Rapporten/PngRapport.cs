﻿using Project.Rapporten;

namespace AvansDevops.Project.Rapporten
{
    public class PngRapport : RapportBase
    {

        public PngRapport(string header, string body)
        {
            this.Header = header;
            this.Body = body;
        }

        public override void PlaceBody()
        {

        }

        public override void PlaceBurndownChart()
        {

        }

        public override void PlaceHeader()
        {

        }

        public override void PlaceSignature()
        {

        }
    }
}