﻿namespace Project.Rapporten
{
    public abstract class RapportBase
    {
        public string? Header { get; set; }
        public string? Body { get;  set; }
        public string? Chart { get; set; }

        public void GenerateRapport()
        {
            PlaceHeader();
            PlaceBody();
            PlaceBurndownChart();
            PlaceSignature();
        }

        public abstract void PlaceHeader();

        public abstract void PlaceBody();

        public abstract void PlaceBurndownChart();

        public abstract void PlaceSignature();

    }
}