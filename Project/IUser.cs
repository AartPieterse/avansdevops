﻿namespace AvansDevops.Project
{
    public interface IUser
    {

        void UpdateSubscribers(string subject, string body);

        string GetEmailAddress();

        string GetName();
    }
}