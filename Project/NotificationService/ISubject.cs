﻿using AvansDevops.Project;

namespace Project.NotificationService
{
    public interface ISubject
    {
        void Subscribe(IUser user);

        void Unsubscribe(IUser user);

        void NotifySubscribers(string subject, string body);
    }
}
