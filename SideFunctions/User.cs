﻿using System.Net.Mail;
using System.Threading.Tasks.Sources;

namespace AvansDevops.SideFunctions
{
    public class User : IUser
    {
        private readonly string _name;
        private readonly string _emailAddress;

        public User(string name, string email)
        {
            this._name = name;
            this._emailAddress = email;
        }

        public string GetName()
        {
            return _name;
        }

        public string GetEmailAddress()
        {
            return _emailAddress;
        }

        public void UpdateSubscribers(string subject, string body)
        {
            string senderEmail = "no_reply@avansdevops.com";
            string receiverEmail = this._emailAddress;

            MailMessage message = new(senderEmail, receiverEmail, subject, body);

            SmtpClient smtpClient = new("smtp.avansdevops.com", 587)
            {
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential("username", "password"),
                EnableSsl = true
            };

            //try
            //{
            //    smtpClient.Send(message);
            //}
            //catch (Exception ex)
            //{
            //    throw new SmtpException(ex.Message);
            //}

            // Print on the console to check functionality, to be deleted later
            Console.WriteLine(subject + ", " + body);
        }
    }
}
