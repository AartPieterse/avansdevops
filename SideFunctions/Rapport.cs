﻿namespace AvansDevops.SideFunctions
{
    public abstract class Rapport
    {
        protected string? Header;
        protected string? Body;
        protected string? Chart = null;

        public void GenerateRapport()
        {
            PlaceHeader();
            PlaceBody();
            PlaceBurndownChart();
            PlaceSignature();
        }

        public abstract void PlaceHeader();
        
        public abstract void PlaceBody();

        public abstract void PlaceBurndownChart();

        public abstract void PlaceSignature();

        public void SetHeader(string header)
        {
            Header = header;
        }

        public void SetBody(string body)
        {
            Body = body;
        }

        public virtual void SetBurndown(string chart)
        {
            Chart = chart;
        }

    }
}