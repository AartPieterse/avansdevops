﻿namespace AvansDevops.SideFunctions
{
    public interface IUser
    {

        void UpdateSubscribers(string subject, string body);
    }
}