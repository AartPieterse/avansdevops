﻿
namespace AvansDevops.SideFunctions
{
    public class EmailService : ISubject
    {
        private IList<IUser> _subscribers;

        public EmailService()
        {
            _subscribers = new List<IUser>();
        }

        public void Subscribe(IUser user)
        {
            _subscribers.Add(user);
        }

        public void Unsubscribe(IUser user)
        {
            _subscribers.Remove(user);
        }

        public void NotifySubscribers(string subject, string body)
        {
            foreach (IUser u in _subscribers)
            {
                u.UpdateSubscribers(subject, body);
            }
        }
    }
}
