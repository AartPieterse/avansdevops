﻿
namespace AvansDevops.SideFunctions.Rapporten
{
    public class HtmlRapport : Rapport
    {

        public HtmlRapport(string header, string body)
        {
            this.Header = header;
            this.Body = body;
        }

        public override void PlaceBody()
        {
            
        }

        public override void PlaceBurndownChart()
        {
            
        }

        public override void PlaceHeader()
        {
           
        }

        public override void PlaceSignature()
        {
            
        }
    }
}
