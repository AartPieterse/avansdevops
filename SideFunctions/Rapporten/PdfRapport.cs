﻿
namespace AvansDevops.SideFunctions.Rapporten
{
    public class PdfRapport : Rapport
    {

        public PdfRapport(string header, string body)
        {
            this.Header = header;
            this.Body = body;
        }

        public override void PlaceBody()
        {

        }

        public override void PlaceBurndownChart()
        {

        }

        public override void PlaceHeader()
        {

        }

        public override void PlaceSignature()
        {

        }
    }
}
